#!/usr/bin/env node
var Module = require("../liba.js")
var NL = -3
var NM = -2
var NS = -1
var ZO = 0
var PS = +1
var PM = +2
var PL = +3
var me = [
    [Module.MF_TRI, NL, NL, NM],
    [Module.MF_TRI, NL, NM, NS],
    [Module.MF_TRI, NM, NS, ZO],
    [Module.MF_TRI, NS, ZO, PS],
    [Module.MF_TRI, ZO, PS, PM],
    [Module.MF_TRI, PS, PM, PL],
    [Module.MF_TRI, PM, PL, PL],
]
var NL = -6
var NM = -4
var NS = -2
var ZO = 0
var PS = +2
var PM = +4
var PL = +6
var mec = [
    [Module.MF_TRI, NL, NL, NM],
    [Module.MF_TRI, NL, NM, NS],
    [Module.MF_TRI, NM, NS, ZO],
    [Module.MF_TRI, NS, ZO, PS],
    [Module.MF_TRI, ZO, PS, PM],
    [Module.MF_TRI, PS, PM, PL],
    [Module.MF_TRI, PM, PL, PL],
]
var NL = -15
var NM = -10
var NS = -5
var ZO = 0
var PS = +5
var PM = +10
var PL = +15
var mkp = [
    [NL, NL, NM, NM, NS, ZO, ZO],
    [NL, NL, NM, NS, NS, ZO, PS],
    [NM, NM, NM, NS, ZO, PS, PS],
    [NM, NM, NS, ZO, PS, PM, PM],
    [NS, NS, ZO, PS, PS, PM, PM],
    [NS, ZO, PS, PM, PM, PM, PL],
    [ZO, ZO, PM, PM, PM, PL, PL],
]
var NL = -3
var NM = -2
var NS = -1
var ZO = 0
var PS = +1
var PM = +2
var PL = +3
var mki = [
    [PL, PL, PM, PM, PS, ZO, ZO],
    [PL, PL, PM, PS, PS, ZO, ZO],
    [PL, PM, PS, PS, ZO, NS, NS],
    [PM, PM, PS, ZO, NS, NM, NM],
    [PM, PS, ZO, NS, NS, NM, NL],
    [ZO, ZO, NS, NS, NM, NL, NL],
    [ZO, ZO, NS, NM, NM, NL, NL],
]
var mkd = [
    [NS, PS, PL, PL, PL, PM, NS],
    [NS, PS, PL, PM, PM, PS, ZO],
    [ZO, PS, PM, PM, PS, PS, ZO],
    [ZO, PS, PS, PS, PS, PS, ZO],
    [ZO, ZO, ZO, ZO, ZO, ZO, ZO],
    [NL, NS, NS, NS, NS, NS, NL],
    [NL, NM, NM, NM, NS, NS, NL],
]

var ctx = new Module.pid_fuzzy()
ctx.rule(me, mec, mkp, mki, mkd)
ctx.kpid(10, 0.1, 1)
ctx.buff(2)
console.log(ctx.iter(10, 0))
ctx.delete()

var ctx = new Module.pid_fuzzy(-10, 10)
ctx.rule(me, mec, mkp, mki, mkd)
ctx.kpid(10, 0.1, 1)
ctx.buff(2)
console.log(ctx.iter(10, 0))
ctx.delete()

var ctx = new Module.pid_fuzzy(-10, 10, 10)
ctx.rule(me, mec, mkp, mki, mkd)
ctx.kpid(10, 0.1, 1)
ctx.buff(2)
console.log(ctx.iter(10, 0))
ctx.delete()
